@groovy.transform.BaseScript com.ibm.dbb.groovy.ScriptLoader baseScript
import groovy.cli.commons.*
import groovy.util.*

def cli = new CliBuilder(usage: 'groovy build.groovy [options]')
cli.srcDir(longOpt: 'sourceDir', args: 1, argName: 'sourceDirectory', required: true, 'Specify the source directory')
cli.wrkDir(longOpt: 'workDir', args: 1, argName: 'workDirectory', required: true, 'Specify the work directory')
cli.hlq(longOpt: 'hlq', args: 1, argName: 'hlq', required: true, 'Specify the source directory')
cli.a(longOpt: 'application', args: 1, argName: 'application', required: false, 'Specify the source directory')

def options = cli.parse(args)

// Print the parsed options
println "Source directory: ${options.srcDir}"

// // Collect the arguments to pass:
// def compileArgs = [options.wrkDir, "COBJAVA" ,options.hlq, "JNI"]  // Assuming options.hlq holds the hlq value

// // Execute compile.groovy with the arguments:
// def result = groovy.util.GroovyScriptEngine.run("compile.groovy", compileArgs)
// println "Compilation result: $result"

String command = "/usr/lpp/IBM/dbb/bin/groovyz ${options.srcDir}/COBcallJava/languages/compile.groovy ${options.srcDir} COBJAVA DBB.ZEN JNI "

// Execute the command using ProcessBuilder and redirect output/error
ProcessBuilder builder = new ProcessBuilder("sh", "-c", command)
builder.redirectErrorStream(true)

Process process = builder.start()
process.waitFor()

// Check the exit code for success 
if (process.exitValue() == 0) {
   String output = process.getInputStream().getText()
   println "Output: $output"
   command = "cat ${options.srcDir}/COBJAVA.log"

// Execute the command using ProcessBuilder and redirect output/error
   builder = new ProcessBuilder("sh", "-c", command)
   builder.redirectErrorStream(true)

   process = builder.start()
   process.waitFor()
   output = process.getInputStream().getText()
   println "Output: $output"

   command = "/usr/lpp/IBM/dbb/bin/groovyz ${options.srcDir}/COBcallJava/languages/link.groovy DBB.ZEN COBJAVA ${options.srcDir}"

// Execute the command using ProcessBuilder and redirect output/error
   builder = new ProcessBuilder("sh", "-c", command)
   builder.redirectErrorStream(true)

   process = builder.start()
   process.waitFor()

   output = process.getInputStream().getText()
   println "Output: $output"

    command = "cat ${options.srcDir}/COBJAVA.lnklog"

// Execute the command using ProcessBuilder and redirect output/error
   builder = new ProcessBuilder("sh", "-c", command)
   builder.redirectErrorStream(true)

   process = builder.start()
   process.waitFor()
   output = process.getInputStream().getText()
   println "Output: $output"

 } else {
   println "Error: Failed to compile"
   // Optionally, read the combined output/error stream for more details
  String output = process.getInputStream().getText()
   println "Output: $output"
 }