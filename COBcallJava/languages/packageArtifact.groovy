import com.ibm.dbb.build.*

println("Packaging the artifact ...")

def hlq = args[0]
def member = args[1]
def artifactDir = args[2]

def copyLog = new CopyToHFS()
copyLog.setDataset("${hlq}.LOAD")
copyLog.setMember("${member}")
copyLog.copyMode(DBBConstants.CopyMode.LOAD)
copyLog.setFile(new File("${artifactDir}/${hlq}.LOAD"))

rc = copyLog.execute()

if (rc > 4) {
    throw new Exception("Return code is greater than 4! RC=$rc")
} else {
    println("Packaging the artifact successful! RC=$rc")
}

// String command = "tar -czvf myProject.tar ${artifactDir}/${hlq}.LOAD"

// // Execute the command using ProcessBuilder and redirect output/error
// ProcessBuilder builder = new ProcessBuilder("sh", "-c", command)
// builder.redirectErrorStream(true)

// Process process = builder.start()
// process.waitFor()

// // Check the exit code for success
// if (process.exitValue() == 0) {
//   println "Log file successfully archived to myProject.tar"
// } else {
//   println "Error: Failed to archive log file"
//   // Optionally, read the combined output/error stream for more details
//   String output = process.getInputStream().getText()
//   println "Output: $output"
// }
